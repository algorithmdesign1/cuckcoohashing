/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.testingcuckcoo;

/**
 *
 * @author ADMIN
 */
public class TestCuckcoo {

    public static void main(String[] args) {

        Cuckcoo<Integer, String> table = new Cuckcoo<Integer, String>(11);
        table.put(25, "Annette");       // 3
        table.showRound();
        System.out.println(table.toString());
        table.put(7, "Laville");        // 7
        table.showRound();
        System.out.println(table.toString());
        table.put(11, "Batman");        // 0
        table.showRound();
        System.out.println(table.toString());
        table.put(12, "Capheny");       // 1
        table.showRound();
        System.out.println(table.toString());
        table.put(13, "Paine");         // 2
        table.showRound();
        System.out.println(table.toString());
        table.put(14, "Yan");           // 3
        table.showRound();
        System.out.println(table.toString());
        table.put(15, "Krixi");         // 4
        table.showRound();
        System.out.println(table.toString());
        table.put(16, "Hayate");        // 5
        table.showRound();
        System.out.println(table.toString());
//        
//        System.out.println();
//        System.out.println("KEYS: " + table.keys());
//        System.out.println("VALUES: " + table.values());
//        
//        System.out.println();
//        table.remove("A", "AA");
//        table.remove("A", "CC");
//        System.out.println(table.toString() +"    " + table.size());

        System.out.println("AFTER REHASH");
        table.rehash();
        System.out.println(table.toString());
//        table.clear();
//        System.out.println(table.toString() +"    " + table.size());
    }
}
