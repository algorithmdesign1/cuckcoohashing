/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.testingcuckcoo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author ADMIN
 */
public class Cuckcoo<K, V> {

    private int size;
    private int a = 37, b = 17;
    private Bucket<K, V>[] table;

    private class Bucket<K, V> {

        private K bucKey = null;
        private V value = null;

        public Bucket(K k, V v) {
            bucKey = k;
            value = v;
        }

        private K getBucKey() {
            return bucKey;
        }
        
        //private void setBucKey(K key) {
        //bucKey = key;
        //}

        private V getValue() {
            return value;
        }
        
        //private void setValue(V value) {
        //this.value = value;
        //}
    }
    
    public Cuckcoo(int size) {
        this.size = size;
        table = new Bucket[this.size];
    }

    /**
     * Get the number of elements in the table
     */
    
    public int countRound() {
        int countR = 0;
        for (int i = 0; i < size; ++i) {
            if (table[i] != null) {
                countR++;
            }
        }
        return countR;
    }

    public void showRound() {
        System.out.println("Current Cycle is " + countRound());
    }

    /**
     * Removes all elements in the table
     */
    public void clear() {
        table = new Bucket[size];
    }

    /**
     * Get a list of all values in the table
     */
    public List<V> values() {
        List<V> allValues = new ArrayList<V>();
        for (int i = 0; i < size; ++i) {
            if (table[i] != null) {
                allValues.add(table[i].getValue());
            }
        }
        return allValues;
    }

    /**
     * Get all the keys in the table
     */
    public Set<K> keys() {
        Set<K> allKeys = new HashSet<K>();
        for (int i = 0; i < size; ++i) {
            if (table[i] != null) {
                allKeys.add(table[i].getBucKey());
            }
        }
        return allKeys;
    }

    /**
     * Adds a key-value pair to the table by means of cuckoo hashing. Insert
     * into either of 2 separate hash indices, if both are full loop to next
     * hashed index and displace the key-value pair there, else loop for n =
     * size times.
     */
    public void put(K key, V value) {
        int index = key.hashCode() % size;
        int index2 = altHashCode(key) % size;

        if (table[index] != null && table[index].getValue().equals(value)) {
            return;
        }
        if (table[index2] != null && table[index2].getValue().equals(value)) {
            return;
        }
        int position = index;
        Bucket<K, V> buck = new Bucket<K, V>(key, value);
        for (int i = 0; i < 3; i++) {
            if (table[position] == null) {
                table[position] = buck;
                return;
            } else {
                Bucket<K, V> copy = table[position];
                table[position] = buck;
                buck = copy;
            }
            if (position == index) {
                position = index2;
            } else {
                position = index;
            }
        }
        rehash();
        put(key, value);
    }

    /**
     * Retrieve a value in O(1) time based on the key b/c it can only be in 1 of
     * 2 locations
     */
    public V get(K key) {
        int index = key.hashCode() % size;
        int index2 = altHashCode(key) % size;
        if (table[index] != null && table[index].getBucKey().equals(key)) {
            return table[index].getValue();
        } else if (table[index2] != null && table[index2].getBucKey().equals(key)) {
            return table[index2].getValue();
        }
        return null;
    }

    public int altHashCode(K key) {
        return a * b + key.hashCode();
    }

    /**
     * Removes this key value pair from the table
     */
    public boolean remove(K key, V value) {
        int index = key.hashCode() % size;
        int index2 = altHashCode(key) % size;
        if (table[index] != null && table[index].getValue().equals(value)) {
            table[index] = null;
            return true;
        } else if (table[index2] != null && table[index2].getValue().equals(value)) {
            table[index2] = null;
            return true;
        }
        return false;
    }

    /**
     * String representation of the table
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        for (int i = 0; i < size; ++i) {
            if (table[i] != null) {
                sb.append("<");
                sb.append(table[i].getBucKey()); //key
                sb.append(", ");
                sb.append(table[i].getValue()); //value
                sb.append("> ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * a method that regrows the hashTable to capacity: 2*old capacity + 1 and
     * reinserts all the key->value pairs.
     */
    public void rehash() {
        Bucket<K, V>[] tableCopy = table.clone();
        int OLD_CAPACITY = size;
        size = (size * 2) + 1;
        table = new Bucket[size];

        for (int i = 0; i < OLD_CAPACITY; ++i) {
            if (tableCopy[i] != null) {
                put(tableCopy[i].getBucKey(), tableCopy[i].getValue());
            }
        }
        //reset alt hash func
        Random gen = new Random();
        a = gen.nextInt(37);
        b = gen.nextInt(17);
    }

}
